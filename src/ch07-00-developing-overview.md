# Developing for Redox Overview

If you are considering contributing to Redox, or if you want to use Redox as a platform for something else, this part of the book will give you the details you need to be successful. Please join us on the [Redox Chat](./ch13-01-chat.md) and have a look at [CONTRIBUTING.md](https://gitlab.redox-os.org/redox-os/redox/blob/master/CONTRIBUTING.md).

Please feel free to jump around as you read through this part of the book and please try out some of the examples.

The following topics are covered here:
- [The Build Process](./ch08-00-build-process.md)
- [Including Programs in Redox](./ch09-00-developing-for-redox.md)
- [More on Contributing](./ch10-00-contributing.md)
- [Best Practices](./ch11-00-best-practices.md)
- [Redox's Take on Using Git](./ch12-00-using-git.md)
- [Communicating with the Redox Team](./ch13-00-communication.md)