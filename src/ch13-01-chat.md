# Chat

The best way to communicate with the Redox team is on Matrix Chat. You can join the [Redox Space](https://matrix.to/#/#redox:matrix.org) and see the  rooms that are available. The [Redox Support Room](https://matrix.to/#/#redox-support:matrix.org) is the best place to get help with building, installing and running Redox. [Redox OS/Dev](https://matrix.to/#/#redox-dev:matrix.org) is for those that wish to contribute to Redox, or who want deeper technical assistance with developing on Redox. There is also [Redox OS/General](https://matrix.to/#/#redox-general:matrix.org) for general discussions about our use of Matrix and other less-technical topics.

Until recently, we have been using a [Mattermost](https://www.mattermost.org/) chat server, which we are currently maintaining for historical purposes. Contact us on Redox OS/General if you need access to the Mattermost server.
