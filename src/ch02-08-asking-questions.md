# Questions, Feedback, Reporting Issues 

Join the [Redox Chat](./ch13-01-chat.md).  It is the best method to **chat with the Redox team**.

Alternatively you can use [our Discourse Redox-os Forum].

If you would like to report Redox Issues, please do so here [Gitlab Redox Project Issues] and click the [New Issue] button.

[our Discourse Redox-os Forum]: https://discourse.redox-os.org/
[Gitlab Redox Project Issues]: https://gitlab.redox-os.org/redox-os/redox/-/issues
[New Issue]: https://gitlab.redox-os.org/redox-os/redox/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=
