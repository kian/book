# Contributing

So you'd like to contribute to make Redox better! Excellent. This chapter can help you to do just that.

Also have a look at our [CONTRIBUTING.md](https://gitlab.redox-os.org/redox-os/redox/blob/master/CONTRIBUTING.md).
