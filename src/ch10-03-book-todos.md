# Book ToDos

There are lots of parts of this book that can use additional content. Your help would be greatly appreciated.

## Redox Architecture Concepts

If you are familiar with microkernel-based architectures and Rust, we could use help with documenting some of the following topics for the book:
- Device Driver concepts and implementation guidelines
- Memory management
- Application start-up
- System Call operation
- Interprocess Communication
- The Graphics Subsystem

## Redox Programs and Components

Redox has many subprojects that combine to provide a complete system. You can help by improving the documentation for those many subprojects, either in their own documentation, if they have it, or here in the book. For those subprojects with their own documentation, you can add links in this book to the documentation, as well as providing some contextual information about how Redox uses each component.